package com.doranco.rmi.chat.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import com.doranco.rmi.chat.IClient;
import com.doranco.rmi.chat.IServer;

public class Server extends UnicastRemoteObject implements IServer {

	protected Server() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	List<IClient> clients = new ArrayList<IClient>();

	@Override
	public IClient getClient(String arg0) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void login(IClient client) throws RemoteException {		
		clients.add(client);
		client.tell("[SERVER] Bienvenue � toi "+ client.getUserName());
		for(IClient iClient: clients) {
			if(!iClient.equals(client))
				client.tell("[SERVER]: " +client.getUserName()+" a rejoint la conversation");
		}
		
	}

	@Override
	public void sendAll(String message, IClient from) throws RemoteException {

		System.out.println("from" + "[" + from.getUserName() + "]: " + message);

		for (IClient client : clients) {
			if (!client.equals(from))
				client.tell("from" + " [" + from.getUserName() + "]: " + message);
		}

	}

}
