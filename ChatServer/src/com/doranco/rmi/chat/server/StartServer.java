package com.doranco.rmi.chat.server;

import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import com.doranco.rmi.chat.IServer;

public class StartServer {

	public static void main(String[] args) {

		try {
			IServer server = new Server();
			
			LocateRegistry.createRegistry(1099);

			String url = "rmi://" + Inet4Address.getLocalHost().getHostAddress() + "/chatServer";
			
			Naming.rebind(url, server);
			
			System.out.println("Le server de chat est d�marr� ...");

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
