package com.doranco.rmi.chat.client;

import java.rmi.RemoteException;

import com.doranco.rmi.chat.IClient;
import com.doranco.rmi.chat.IServer;

public class Client implements IClient {

	private String userName;
	private IServer server;

	public Client(String userName, IServer server) {
		super();
		this.userName = userName;
		this.server = server;
	}

	protected Client() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getUserName() throws RemoteException {
		return userName;
	}

	@Override
	public void tell(String message) {
		System.out.println(message);
	}

}
