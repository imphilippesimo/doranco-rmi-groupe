package com.doranco.rmi.chat.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

import com.doranco.rmi.chat.IClient;
import com.doranco.rmi.chat.IServer;

public class StartClient {

	public static void main(String[] args) {

		try {

			Remote r = Naming.lookup("rmi://127.0.0.1/chatServer");

			// recuperation de l'objet distant
			IServer serveur = (IServer) r;

			// definition du username
			System.out.println("Entrez votre nom pour vous " + ""
					+ "connecter et signaler votre pr�sence");
			Scanner sc = new Scanner(System.in);
			String userName = sc.nextLine();

			IClient client = new Client(userName, serveur);
			UnicastRemoteObject.exportObject(client, 1098);

			// connexion
			serveur.login(client);

			// diffusion de message
			for (;;) {
				Scanner sci = new Scanner(System.in);
				String message = sci.nextLine();

				serveur.sendAll(message, client);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
